import {
    bindAddNewEventModal,
    bindDeleteEventButton,
    bindEditEventButton,
    bindEditEventModal,
    bindSaveNewEventButton,
} from './my-calendar';
import {getSelectedOption} from './common/utils';

export const bindRoomCalendar = () => {
    const roomsPage = document.getElementById('rooms');
    if (roomsPage) {
        bindRomSelector();
        bindEditEventModal('rooms-clickable');
        bindAddNewEventModal('js-rooms-day');
        bindSaveNewEventButton();
        bindEditEventButton();
        bindDeleteEventButton();
    }
};

const bindRomSelector = () => {
    const roomCalendarSelector = document.getElementById('roomSelectCalendar');

    if (roomCalendarSelector) {
        roomCalendarSelector.addEventListener('change', function () {
            getRoomCalendar(roomCalendarSelector);
        })
    }
};

const getRoomCalendar = (selector) => {
    const selectedValue = selector.options[selector.selectedIndex].value;

    if (selectedValue !== 'undefined') {
        window.location.replace('/rooms?room=' + getSelectedOption(selector));
    }
};