export const bindProjectCalendar = () => {
    bindProjectSelector();
};

export const bindProjectSelector = () => {
    const projectCalendarSelector = document.getElementById('projectSelectCalendar');

    if (projectCalendarSelector) {
        projectCalendarSelector.addEventListener('change', function () {
            getProjectCalendar(projectCalendarSelector);
        })
    }
};

const getProjectCalendar = (selector) => {
    const selectedValue = selector.options[selector.selectedIndex].value;

    if (selectedValue !== 'undefined') {
        window.location.replace('/projects?project=' + selector.options[selector.selectedIndex].value);
    }
};