export const bindRegisterLoginAutoFill = () => {
    bindFirstNameFilling();
    bindLastNameFilling();
};

const bindFirstNameFilling = () => {
    const firstName = document.getElementById('first-name-register');
    const loginField = document.getElementById('login-register');

    if (firstName) {
        firstName.addEventListener('input', () => {
            fillDot(loginField);
            autofillLogin(loginField, firstName.value, true);
            cleanUpDot(loginField);
        });
    }
};

const bindLastNameFilling = () => {
    const lastName = document.getElementById('last-name-register');
    const loginField = document.getElementById('login-register');

    if (lastName) {
        lastName.addEventListener('input', () => {
            fillDot(loginField);
            autofillLogin(loginField, lastName.value, false);
            cleanUpDot(loginField);
        });
    }
};

const autofillLogin = (loginField, textToAdd, isFirst) => {
    if (loginField.value.includes('.')) {
        const firstDotLast = loginField.value.split('.');

        if (firstDotLast.length === 2) {
            if (isFirst) {
                loginField.value = textToAdd + '.' + firstDotLast[1];
            } else {
                loginField.value = firstDotLast[0] + '.' + textToAdd;
            }
        }
    }
};

const fillDot = (loginField) => {
    if (loginField.value === '') {
        loginField.value = '.';
    }
};

const cleanUpDot = (loginField) => {
    if (loginField.value === '.') {
        loginField.value = '';
    }
};