package pkotwica.inz.calendars.user.validator;

import pkotwica.inz.calendars.user.validator.constraint.ChangePasswordConstraint;
import pkotwica.inz.calendars.user.web.form.ChangePassForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ChangePasswordValidator implements ConstraintValidator<ChangePasswordConstraint, ChangePassForm> {
    @Override
    public void initialize(ChangePasswordConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(ChangePassForm changePassForm, ConstraintValidatorContext constraintValidatorContext) {
        return changePassForm.getNewPass().equals(changePassForm.getNewPassRep());
    }
}
