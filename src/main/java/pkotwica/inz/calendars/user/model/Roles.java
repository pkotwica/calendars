package pkotwica.inz.calendars.user.model;

public enum Roles {
    ADMIN, MODERATOR, USER
}
