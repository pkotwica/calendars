package pkotwica.inz.calendars.user.service;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.registration.form.*;
import pkotwica.inz.calendars.user.model.Role;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.repo.RoleRepository;
import pkotwica.inz.calendars.user.repo.UserRepository;
import pkotwica.inz.calendars.user.web.form.ChangeLoginForm;
import pkotwica.inz.calendars.user.web.form.ChangePassForm;
import pkotwica.inz.calendars.user.web.form.UserWithEmail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final static String EMAIL_ALERT = "EMAIL_ERR";
    private final static String LOGIN_ALERT = "LOGIN_ERR";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public Collection<String> addNewUser(RegisterForm registerForm) {
        List<String> errorList = new ArrayList<>();

        if (userRepository.findUserByLogin(registerForm.getLogin()).isPresent()) {
            errorList.add(LOGIN_ALERT);
        }
        if (userRepository.findUserByEmail(registerForm.getEmail()).isPresent()) {
            errorList.add(EMAIL_ALERT);
        }
        if (errorList.isEmpty()) {
            Role role = roleRepository.getByName(registerForm.getRole());
            userRepository.save(User.of(registerForm, role, passwordEncoder.encode(registerForm.getPassword())));
        }

        return errorList;
    }

    public UserData getUserData() {
        User loggedUser = getLoggedUser();
        return new UserData(loggedUser);
    }

    public Optional<User> getUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public Collection<String> getAllUserRoles() {
        return roleRepository.findAll().stream().filter(r -> !r.getName().equals(Roles.ADMIN.name())).map(Role::getName).collect(Collectors.toList());
    }

    public boolean changeLogin(ChangeLoginForm changeLoginForm) {
        if (userRepository.findUserByLogin(changeLoginForm.getNewLogin()).isPresent()) {
            return false;
        }

        User loggedUser = getLoggedUser();
        loggedUser.setLogin(changeLoginForm.getNewLogin());
        userRepository.save(loggedUser);
        return true;
    }

    public boolean changePass(ChangePassForm changePassForm) {
        if (changePassForm.getNewPass().equals(changePassForm.getNewPassRep())) {
            User loggedUser = getLoggedUser();
            loggedUser.setPass(passwordEncoder.encode(changePassForm.getNewPass()));
            userRepository.save(loggedUser);
            return true;
        }

        return false;
    }

    public User getLoggedUser() {
        String loggedUserLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findUserByLogin(loggedUserLogin).orElseThrow(() -> new UsernameNotFoundException("User by login not found: " + loggedUserLogin));
    }

    public List<UserWithEmail> getUsersWithEmails() {
        ArrayList<UserWithEmail> usersWithEmails = new ArrayList<>();

        userRepository.findAllByOrderByLastName().stream().filter(u -> !u.getRole().getName().equals(Roles.ADMIN.name()))
                .forEach(u -> usersWithEmails.add(UserWithEmail.builder().name(u.getLastName() + " " + u.getFirstName()).email(u.getEmail()).build()));

        return usersWithEmails;
    }

    public boolean editUserData(EditUserDataForm editUserDataForm) {
        Optional<User> editableUserOptional = userRepository.findUserByEmail(editUserDataForm.getOldUserEmail());
        if (editableUserOptional.isPresent()) {
            User editableUser = editableUserOptional.get();

            List<User> inconsistentUsers = userRepository.findUsersByLoginOrEmail(editUserDataForm.getLogin(), editUserDataForm.getEmail()).stream().filter(u -> u.getId() != editableUser.getId()).collect(Collectors.toList());
            if (!inconsistentUsers.isEmpty()) {
                return false;
            }

            editableUser.setRole(roleRepository.getByName(editUserDataForm.getRole()));
            editableUser.setFirstName(editUserDataForm.getFirstName());
            editableUser.setLastName(editUserDataForm.getLastName());
            editableUser.setLogin(editUserDataForm.getLogin());
            editableUser.setEmail(editUserDataForm.getEmail());
            editableUser.setJobPosition(editUserDataForm.getJobPosition());

            userRepository.save(editableUser);
            return true;
        }
        return false;
    }

    public boolean editUserPassword(EditUserPasswordForm editUserPasswordForm) {
        if (!editUserPasswordForm.getNewPassword().equals(editUserPasswordForm.getRepeatPassword())) {
            return false;
        }

        Optional<User> user = userRepository.findUserByEmail(editUserPasswordForm.getUserEmail());
        if (user.isPresent()) {
            user.get().setPass(passwordEncoder.encode(editUserPasswordForm.getNewPassword()));
            userRepository.save(user.get());
            return true;
        }

        return false;
    }

    public boolean removeUser(RemoveUserForm removeUserForm) {
        Optional<User> user = userRepository.findUserByEmail(removeUserForm.getUserEmail());
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return true;
        }
        return false;
    }

    public List<User> getRegisteredUsers() {
        return userRepository.findAll().stream().filter(u -> !u.getRole().getName().equals(Roles.ADMIN.name())).collect(Collectors.toList());
    }
}
