package pkotwica.inz.calendars.user.web;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.user.web.factory.ChangeUserDataResponseFactory;
import pkotwica.inz.calendars.user.web.form.ChangeLoginForm;
import pkotwica.inz.calendars.user.web.form.ChangePassForm;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "change-user-data", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ChangeUserDataController {

    private final UserService userService;

    public ChangeUserDataController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("login")
    public CalendarsApiResponse changeLogin(@Valid @RequestBody ChangeLoginForm changeLoginForm, BindingResult bindingResult) {
        return ChangeUserDataResponseFactory.changeLoginResponse(!bindingResult.hasErrors() && userService.changeLogin(changeLoginForm));
    }

    @PostMapping("pass")
    public CalendarsApiResponse changePass(@Valid @RequestBody ChangePassForm changePassForm, BindingResult bindingResult) {
        return ChangeUserDataResponseFactory.changePassResponse(!bindingResult.hasErrors() && userService.changePass(changePassForm));
    }
}
