package pkotwica.inz.calendars.user.web.form;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
public class ChangeLoginForm {
    @NotNull
    @NotEmpty
    private String newLogin;

    public String getNewLogin() {
        return newLogin;
    }
}
