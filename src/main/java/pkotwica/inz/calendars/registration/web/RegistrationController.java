package pkotwica.inz.calendars.registration.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pkotwica.inz.calendars.registration.form.RegisterForm;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("register")
public class RegistrationController {
    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getRegisterPage(@ModelAttribute RegisterForm registerForm) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }
        return "admin/register";
    }

    @GetMapping("users")
    public String getUsersPage(@ModelAttribute RegisterForm registerForm, Model model) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }

        model.addAttribute("users", userService.getRegisteredUsers());
        return "admin/users";
    }

    @PostMapping
    public String registerUser(@Valid @ModelAttribute RegisterForm registerForm, BindingResult bindingResult, Model model) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("error",true);
        } else if (passwordIsNotAccepted(registerForm)) {
            model.addAttribute("errorPassword",true);
        } else {
            Collection<String> saveErr  = userService.addNewUser(registerForm);
            if (saveErr.isEmpty()) {
                model.addAttribute("success", true);
            } else {
                for (String err : saveErr) {
                    model.addAttribute(err, true);
                }
            }
        }

        return "admin/register";
    }

    private boolean passwordIsNotAccepted(RegisterForm registerForm) {
        return !registerForm.getPassword().equals(registerForm.getPasswordRep());
    }

    private boolean isNotUser() {
        return Roles.USER.name().equals(userService.getUserData().getRole());
    }

    @ModelAttribute(name = "userRoles")
    public Collection<String> getAllUserRoles() {
        return userService.getAllUserRoles();
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
