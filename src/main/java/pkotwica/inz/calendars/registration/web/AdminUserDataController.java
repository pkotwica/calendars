package pkotwica.inz.calendars.registration.web;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import pkotwica.inz.calendars.registration.factory.EditUserDataResponseFactory;
import pkotwica.inz.calendars.registration.form.EditUserDataForm;
import pkotwica.inz.calendars.registration.form.EditUserPasswordForm;
import pkotwica.inz.calendars.registration.form.RemoveUserForm;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;

@RestController
@RequestMapping("admin-user")
public class AdminUserDataController {

    private final UserService userService;

    public AdminUserDataController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("edit")
    public CalendarsApiResponse editUserData(@Valid @RequestBody EditUserDataForm editUserDataForm, BindingResult bindingResult) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }

        if (bindingResult.hasErrors()) {
            return EditUserDataResponseFactory.fillAllError();
        }
        return EditUserDataResponseFactory.ediByIsSuccess(userService.editUserData(editUserDataForm));
    }

    @PostMapping("password")
    public CalendarsApiResponse editUserPassword(@Valid @RequestBody EditUserPasswordForm editUserPasswordForm, BindingResult bindingResult) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }

        if (bindingResult.hasErrors()) {
            return EditUserDataResponseFactory.fillAllError();
        }
        return EditUserDataResponseFactory.passByIsSuccess(userService.editUserPassword(editUserPasswordForm));
    }

    @PostMapping("remove")
    public CalendarsApiResponse remove(@Valid @RequestBody RemoveUserForm removeUserForm, BindingResult bindingResult) {
        if (isNotUser()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found such mapping for you");
        }

        if (bindingResult.hasErrors()) {
            return EditUserDataResponseFactory.fillAllError();
        }
        return EditUserDataResponseFactory.removeByIsSuccess(userService.removeUser(removeUserForm));
    }

    private boolean isNotUser() {
        return Roles.USER.name().equals(userService.getUserData().getRole());
    }
}
