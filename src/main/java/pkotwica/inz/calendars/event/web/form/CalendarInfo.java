package pkotwica.inz.calendars.event.web.form;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalendarInfo {
    private Month currentMonth;
    private Month prevMonth;
    private Month nextMonth;
    private Integer prevYear;
    private Integer nextYear;
    private Integer currentYear;
    private Integer startEmptyDays;
    private Integer endEmptyDays;
    private List<Day> days;
}
