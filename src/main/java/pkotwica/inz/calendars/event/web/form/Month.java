package pkotwica.inz.calendars.event.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Month {
    public String name;
    public int number;

    public static Month of(Months month) {
        return new Month(month.getName(), month.getNumber());
    }
}
