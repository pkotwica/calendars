package pkotwica.inz.calendars.event.web;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.event.web.factory.EditEventResponseFactory;
import pkotwica.inz.calendars.event.web.form.DeleteEventForm;
import pkotwica.inz.calendars.event.web.form.SaveEventForm;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "event", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("new")
    public CalendarsApiResponse saveNewEvent(@Valid @RequestBody SaveEventForm saveEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllDataErrEvent();
        }

        if (eventService.checkIsSpareRoom(saveEventForm)) {
            return EditEventResponseFactory.saveByIsSuccess(eventService.saveNewEvent(saveEventForm));
        } else {
            return EditEventResponseFactory.roomBooked();
        }
    }

    @PostMapping("edit")
    public CalendarsApiResponse editEvent(@Valid @RequestBody SaveEventForm saveEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllDataErrEvent();
        }

        if (eventService.checkIsSpareRoom(saveEventForm)) {
            return EditEventResponseFactory.saveByIsSuccess(eventService.editEvent(saveEventForm));
        } else {
            return EditEventResponseFactory.roomBooked();
        }
    }

    @PostMapping("delete")
    public CalendarsApiResponse deleteEvent(@Valid @RequestBody DeleteEventForm deleteEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllError();
        }
        return EditEventResponseFactory.deleteByIsSuccess(eventService.deleteEvent(deleteEventForm));
    }
}
