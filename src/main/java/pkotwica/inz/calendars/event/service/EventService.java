package pkotwica.inz.calendars.event.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pkotwica.inz.calendars.event.model.Event;
import pkotwica.inz.calendars.event.model.EventType;
import pkotwica.inz.calendars.event.model.EventTypes;
import pkotwica.inz.calendars.event.repo.EventRepository;
import pkotwica.inz.calendars.event.repo.EventTypeRepository;
import pkotwica.inz.calendars.event.web.form.*;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.repo.UserRepository;
import pkotwica.inz.calendars.user.service.UserService;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class EventService {
    private final EventRepository eventRepository;
    private final EventTypeRepository eventTypeRepository;
    private final UserService userService;
    private final ProjectService projectService;
    private final RoomService roomService;
    private final UserRepository userRepository; //TODO:PK user repo shouldn't be here

    public EventService(EventRepository eventRepository, UserService userService, EventTypeRepository eventTypeRepository, UserRepository userRepository, ProjectService projectService, RoomService roomService) {
        this.eventRepository = eventRepository;
        this.userService = userService;
        this.eventTypeRepository = eventTypeRepository;
        this.userRepository = userRepository;
        this.projectService = projectService;
        this.roomService = roomService;
    }

    public List<String> getEventTypes() {
        return eventTypeRepository.findAll().stream().map(EventType::getType).collect(Collectors.toList());
    }

    public void addUserEvent(String userLogin, EventTypes eventTypeName, Event event) {
        Optional<User> userByLogin = userRepository.findUserByLogin(userLogin);//TODO:PK !!!!

        event.setUser(userByLogin.get());
        event.setType(eventTypeRepository.findEventTypeByType(eventTypeName.name()).get());

        eventRepository.save(event);
    }

    public void addUserEvent(String userLogin, EventTypes eventTypeName, Event event, String roomName) {
        Optional<User> userByLogin = userRepository.findUserByLogin(userLogin);//TODO:PK !!!!

        event.setUser(userByLogin.get());
        event.setType(eventTypeRepository.findEventTypeByType(eventTypeName.name()).get());
        event.setRoom(roomService.getRoomByName(roomName).get());
        eventRepository.save(event);
    }

    public boolean saveNewEvent(SaveEventForm saveEventForm) {
        return saveNewEvent(Event.of(new Event(), saveEventForm), saveEventForm);
    }

    public boolean editEvent(SaveEventForm saveEventForm) {
        if (!isEventBelongsToLoggedUser(saveEventForm.getEventId())) {
            return false;
        } else {
            Optional<Event> event = eventRepository.findEventById(saveEventForm.getEventId());
            event.ifPresent(e -> {
                List<EventType> eventTypes = eventTypeRepository.findAll();
                e.setType(eventTypes.stream().filter(t -> t.getType().equals(saveEventForm.getEventType())).findFirst().orElse(eventTypes.get(0)));
                Event.of(e, saveEventForm);
                Optional<Room> room = roomService.getRoomByName(saveEventForm.getRoomName());
                if (room.isPresent()) {
                    e.setRoom(room.get());
                } else {
                    e.setRoom(null);
                }
                eventRepository.save(e);
            });
            return true;
        }

    }

    public boolean deleteEvent(DeleteEventForm deleteEventForm) {
        if (!isEventBelongsToLoggedUser(deleteEventForm.getEventId())) {
            return false;
        } else {
            eventRepository.deleteById(deleteEventForm.getEventId());
            return true;
        }
    }

    public CalendarInfo getCalendarInfo(Integer month, Integer year) {
        return getCalendarInfo(month, year, Collections.singletonList(userService.getLoggedUser()));
    }

    @Transactional
    public CalendarInfo getCalendarInfoByProject(Integer month, Integer year, String projectName) {
        List<User> usersByProject = projectService.getUsersByProject(projectName);
        User loggedUser = userService.getLoggedUser();

        if (usersByProject.contains(loggedUser) || !loggedUser.getRole().getName().equals(Roles.USER.name())) {
            return getCalendarInfo(month, year, usersByProject);
        } else {
            return getCalendarInfo(month, year, Collections.emptyList());
        }
    }

    public CalendarInfo getCalendarInfoByRoom(Integer month, Integer year, String roomName) {
        Optional<Room> roomByName = roomService.getRoomByName(roomName);

        CalendarInfo calendarInfo = getCalendar(month, year);


        return roomByName.map(room -> getCalendarInfoWithEvents(calendarInfo, eventRepository.findEventsByRoomAndStartDateBetweenOrderByStartDate(
                room,
                LocalDateTime.of(calendarInfo.getCurrentYear(), calendarInfo.getCurrentMonth().getNumber(), 1, 0, 0),
                LocalDateTime.of(calendarInfo.getCurrentYear(), calendarInfo.getCurrentMonth().getNumber(), calendarInfo.getDays().size(), 0, 0))))
                .orElse(calendarInfo);
    }

    public CalendarInfo getCalendarInfoByUser(Integer month, Integer year, String email) {
        Optional<User> user = userService.getUserByEmail(email);
        return user.map(u -> getCalendarInfo(month, year, Collections.singletonList(u))).orElse(null);
    }

    @Transactional
    public boolean checkIsSpareRoom(SaveEventForm saveEventForm) {
        LocalDateTime startDate = LocalDateTime.of(saveEventForm.getYear(), saveEventForm.getMonth(), saveEventForm.getDay(), saveEventForm.getStartHour(), saveEventForm.getStartMinute());
        LocalDateTime endDate = LocalDateTime.of(saveEventForm.getYear(), saveEventForm.getMonth(), saveEventForm.getDay(), saveEventForm.getEndHour(), saveEventForm.getEndMinute());

        return eventRepository.findEventsByRoom_NameAndStartDateBetweenOrEndDateBetween(
                saveEventForm.getRoomName(),
                startDate,
                endDate,
                startDate,
                endDate
        ).isEmpty();
    }

    private boolean isEventBelongsToLoggedUser(Long eventId) {
        Optional<Event> event = eventRepository.findEventById(eventId);
        AtomicBoolean isBelong = new AtomicBoolean(false);
        event.ifPresent(e -> isBelong.set(e.getUser().getId() == userService.getLoggedUser().getId()));
        return isBelong.get();
    }

    private boolean saveNewEvent(Event event, SaveEventForm saveEventForm) {
        event.setUser(userService.getLoggedUser());
        List<EventType> eventTypes = eventTypeRepository.findAll();
        event.setType(eventTypes.stream().filter(t -> t.getType().equals(saveEventForm.getEventType())).findFirst().orElse(eventTypes.get(0)));
        roomService.getRoomByName(saveEventForm.getRoomName()).ifPresent(event::setRoom);
        Event saved = eventRepository.save(event);
        return saved != null;
    }

    private CalendarInfo getCalendarInfo(Integer month, Integer year, List<User> users) {
        CalendarInfo calendarInfo = getCalendar(month, year);
        populateEventsInCalendarInfo(calendarInfo, users);
        return calendarInfo;
    }

    private CalendarInfo getCalendarInfoWithEvents(CalendarInfo calendarInfo, List<Event> events) {
        events.forEach(e -> calendarInfo.getDays().stream().filter(d -> d.getNumber().equals(e.getStartDate().getDayOfMonth())).forEach(d -> d.getEvents().add(e)));
        return calendarInfo;
    }

    private CalendarInfo getCalendar(Integer month, Integer year) {
        Calendar calendar = Calendar.getInstance();

        if (month == null) {
            month = calendar.get(Calendar.MONTH) + 1;
        }
        if (year == null) {
            year = calendar.get(Calendar.YEAR);
        }

        CalendarInfo calendarInfo = CalendarInfo.builder()
                .prevMonth(Month.of(Months.getMonthByNumber(month - 1)))
                .currentMonth(Month.of(Months.getMonthByNumber(month)))
                .nextMonth(Month.of(Months.getMonthByNumber(month + 1)))
                .build();


        populateYearInCalendarInfo(calendarInfo, year);
        populateDaysInCalendarInfo(calendarInfo);

        return calendarInfo;
    }

    private void populateEventsInCalendarInfo(CalendarInfo calendarInfo, List<User> users) {
        List<Event> currentEvents = eventRepository.findEventsByUserInAndStartDateBetweenOrderByStartDate(
                users,
                LocalDateTime.of(calendarInfo.getCurrentYear(), calendarInfo.getCurrentMonth().getNumber(), 1, 0, 0),
                LocalDateTime.of(calendarInfo.getCurrentYear(), calendarInfo.getCurrentMonth().getNumber(), calendarInfo.getDays().size(), 0, 0));

        currentEvents.forEach(e -> calendarInfo.getDays().stream().filter(d -> d.getNumber().equals(e.getStartDate().getDayOfMonth())).forEach(d -> d.getEvents().add(e)));
    }

    private void populateDaysInCalendarInfo(CalendarInfo calendarInfo) {
        List<Day> days = new ArrayList<>();
        YearMonth yearMonthObject = YearMonth.of(calendarInfo.getCurrentYear(), calendarInfo.getCurrentMonth().getNumber());
        int daysInMonth = yearMonthObject.lengthOfMonth();

        IntStream.rangeClosed(1, daysInMonth).forEach(i -> {
            int numberOfWeekDay = yearMonthObject.atDay(i).getDayOfWeek().getValue();
            days.add(Day.builder().number(i).numberOfWeekDay(numberOfWeekDay).name(Days.getNameByNumber(numberOfWeekDay)).events(new ArrayList<>()).build());
        });

        calendarInfo.setStartEmptyDays(yearMonthObject.atDay(1).getDayOfWeek().getValue() - 1);
        calendarInfo.setEndEmptyDays(7 - yearMonthObject.atDay(daysInMonth).getDayOfWeek().getValue());
        calendarInfo.setDays(days);
    }

    private void populateYearInCalendarInfo(CalendarInfo calendarInfo, Integer year) {
        calendarInfo.setNextYear(prepareNextYear(calendarInfo.getNextMonth().getNumber(), year));
        calendarInfo.setPrevYear(preparePrevYear(calendarInfo.getPrevMonth().getNumber(), year));
        calendarInfo.setCurrentYear(year);
    }

    private Integer prepareNextYear(Integer month, Integer year) {
        return month.equals(1) ? year + 1 : year;
    }

    private Integer preparePrevYear(Integer month, Integer year) {
        return month.equals(12) ? year - 1 : year;
    }
}
