package pkotwica.inz.calendars.room.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

@Controller
@RequestMapping
public class RoomController {

    private final UserService userService;
    private final RoomService roomService;
    private final EventService eventService;

    public RoomController(UserService userService, RoomService roomService, EventService eventService) {
        this.userService = userService;
        this.roomService = roomService;
        this.eventService = eventService;
    }

    @GetMapping({"rooms/{month}/{year}", "rooms/{month}", "rooms"})
    public String rooms(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String room, Model model) {
        if (room != null) {
            model.addAttribute("selectedRoom", room);
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByRoom(month, year, room));
            model.addAttribute("loggedUserId", userService.getLoggedUser().getId());
            model.addAttribute("eventTypes", eventService.getEventTypes());
        }
        model.addAttribute("rooms", roomService.getAllRoomsData());

        return "rooms";
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
