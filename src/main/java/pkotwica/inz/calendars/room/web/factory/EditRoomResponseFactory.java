package pkotwica.inz.calendars.room.web.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class EditRoomResponseFactory extends CalendarsApiResponseFactory {
    private final static String EDIT_ROOM_FAILED_MESSAGE = "Filled data may exists or are incorrect. Try another.";
    private final static String REMOVE_FAILED_MESSAGE = "Something goes wrong. Contact with support.";

    public static CalendarsApiResponse addByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, EDIT_ROOM_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse editByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, EDIT_ROOM_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse removeByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, REMOVE_FAILED_MESSAGE);
    }
}
