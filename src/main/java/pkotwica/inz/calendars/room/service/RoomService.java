package pkotwica.inz.calendars.room.service;

import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.room.repo.RoomRepository;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.room.web.form.RoomData;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public List<RoomData> getAllRoomsData() {
        return roomRepository.findAll().stream().map(r -> RoomData.builder().name(r.getName()).number(r.getNumber()).build()).collect(Collectors.toList());
    }

    public Optional<Room> getRoomByName(String name) {
        return roomRepository.findByName(name);
    }

    public boolean addNewRoom(EditRoomForm editRoomForm) {
        if (roomRepository.findRoomsByNameOrNumber(editRoomForm.getRoomName(), editRoomForm.getRoomNumber()).isEmpty()) {
            roomRepository.save(Room.builder().name(editRoomForm.getRoomName()).number(editRoomForm.getRoomNumber()).build());
            return true;
        }

        return false;
    }

    public boolean editRoom(EditRoomForm editRoomForm) {
        Optional<Room> changingRoom = roomRepository.findByName(editRoomForm.getOldName());
        List<Room> matchingRooms = roomRepository.findRoomsByNameOrNumber(editRoomForm.getRoomName(), editRoomForm.getRoomNumber());

        if (changingRoom.isPresent() && matchingRooms.stream().noneMatch(r -> r.getId() != changingRoom.get().getId())) {
           changingRoom.get().setName(editRoomForm.getRoomName());
           changingRoom.get().setNumber(editRoomForm.getRoomNumber());
           roomRepository.save(changingRoom.get());
           return true;
        }

        return false;
    }

    public boolean removeRoom(String room) {
        Optional<Room> changingRoom = roomRepository.findByName(room);

        if (changingRoom.isPresent()) {
            roomRepository.delete(changingRoom.get());
            return true;
        }

        return false;
    }
}
