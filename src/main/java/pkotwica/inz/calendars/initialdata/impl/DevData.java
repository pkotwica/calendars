package pkotwica.inz.calendars.initialdata.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pkotwica.inz.calendars.event.model.Event;
import pkotwica.inz.calendars.event.model.EventTypes;
import pkotwica.inz.calendars.event.repo.EventRepository;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.initialdata.InitialData;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.registration.form.RegisterForm;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.room.repo.RoomRepository;
import pkotwica.inz.calendars.user.service.UserService;

import java.time.LocalDateTime;

@Component
@Profile("dev")
public class DevData implements InitialData {

    private final EssentialData essentialData;
    private final UserService userService;
    private final EventService eventService;
    private final ProjectService projectService;
    private final RoomRepository roomRepository;
    private final EventRepository eventRepository;

    public DevData(UserService userService, EssentialData essentialData, EventService eventService, ProjectService projectService, RoomRepository roomRepository, EventRepository eventRepository) {
        this.essentialData = essentialData;
        this.userService = userService;
        this.eventService = eventService;
        this.projectService = projectService;
        this.roomRepository = roomRepository;
        this.eventRepository = eventRepository;
    }

    @Override
    public void load() {
        essentialData.load();
        addUsers();
        addEvents();
        addProjects();
        addRooms();
        addRoomsEvents();
    }

    private void addRoomsEvents() {
        eventService.addUserEvent("login", EventTypes.HOLIDAYS, Event.builder().label("Sample Holiday Event").startDate(LocalDateTime.of(2019, 10, 21, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build(), "RoomThree");
        eventService.addUserEvent("login", EventTypes.HOLIDAYS, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 15, 9, 30)).endDate(LocalDateTime.of(2019, 10, 15, 14, 20)).build(), "RoomTwo");
        eventService.addUserEvent("login", EventTypes.OFFICE, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 1, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build(), "RoomOne");
    }

    private void addRooms() {
        roomRepository.save(Room.builder().name("RoomOne").number("1").build());
        roomRepository.save(Room.builder().name("RoomTwo").number("2").build());
        roomRepository.save(Room.builder().name("RoomThree").number("3").build());
    }

    private void addProjects() {
        projectService.addNewProject(new EditProjectForm("ProjectOne"));
        projectService.addUserToProject(new EditUserInProjectForm("ProjectOne", "piotr.kotwica@email.com"));
        projectService.addUserToProject(new EditUserInProjectForm("ProjectOne", "marcin.jarosinski@email.com"));
    }

    private void addUsers() {
        userService.addNewUser(new RegisterForm("Piotr", "Kotwica", "login", "Developer", "piotr.kotwica@email.com", "USER", "pass", "pass"));
        userService.addNewUser(new RegisterForm("Marcin", "Jarosinski", "mar", "Developer", "marcin.jarosinski@email.com", "USER", "cin", "cin"));
    }

    private void addEvents() {
        eventService.addUserEvent("login", EventTypes.HOLIDAYS, Event.builder().label("Sample Holiday Event").startDate(LocalDateTime.of(2019, 10, 21, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build());
        eventService.addUserEvent("login", EventTypes.HOLIDAYS, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 15, 9, 30)).endDate(LocalDateTime.of(2019, 10, 15, 14, 20)).build());
        eventService.addUserEvent("login", EventTypes.OFFICE, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 1, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build());
        eventService.addUserEvent("login", EventTypes.PRIVATE, Event.builder().label("Sample Private Event").startDate(LocalDateTime.of(2019, 10, 31, 9, 30)).endDate(LocalDateTime.of(2019, 10, 15, 14, 20)).build());
        eventService.addUserEvent("login", EventTypes.OFFICE, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 9, 13, 10, 0)).endDate(LocalDateTime.of(2019, 9, 21, 12, 12)).build());
        eventService.addUserEvent("login", EventTypes.PRIVATE, Event.builder().label("Sample Private Event").startDate(LocalDateTime.of(2019, 11, 15, 9, 30)).endDate(LocalDateTime.of(2019, 11, 15, 14, 20)).build());
        eventService.addUserEvent("mar", EventTypes.HOLIDAYS, Event.builder().label("Sample Holiday Event").startDate(LocalDateTime.of(2019, 10, 22, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build());
        eventService.addUserEvent("mar", EventTypes.HOLIDAYS, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 16, 9, 30)).endDate(LocalDateTime.of(2019, 10, 15, 14, 20)).build());
        eventService.addUserEvent("mar", EventTypes.OFFICE, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 10, 2, 10, 0)).endDate(LocalDateTime.of(2019, 10, 21, 12, 12)).build());
        eventService.addUserEvent("mar", EventTypes.PRIVATE, Event.builder().label("Sample Private Event").startDate(LocalDateTime.of(2019, 10, 25, 9, 30)).endDate(LocalDateTime.of(2019, 10, 15, 14, 20)).build());
        eventService.addUserEvent("mar", EventTypes.OFFICE, Event.builder().label("Sample Office Event").startDate(LocalDateTime.of(2019, 9, 14, 10, 0)).endDate(LocalDateTime.of(2019, 9, 21, 12, 12)).build());
        eventService.addUserEvent("mar", EventTypes.PRIVATE, Event.builder().label("Sample Private Event").startDate(LocalDateTime.of(2019, 11, 16, 9, 30)).endDate(LocalDateTime.of(2019, 11, 15, 14, 20)).build());
    }
}
