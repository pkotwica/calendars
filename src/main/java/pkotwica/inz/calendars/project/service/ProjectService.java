package pkotwica.inz.calendars.project.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pkotwica.inz.calendars.project.model.Project;
import pkotwica.inz.calendars.project.repo.ProjectRepository;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.project.web.form.ProjectData;
import pkotwica.inz.calendars.project.web.form.UserProjectData;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.user.web.form.UserWithEmail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final UserService userService;

    public ProjectService(ProjectRepository projectRepository, UserService userService) {
        this.projectRepository = projectRepository;
        this.userService = userService;
    }

    public List<ProjectData> getAllProjectsData() {
        ArrayList<ProjectData> projects = new ArrayList<>();
        projectRepository.findAllByOrderByName().forEach(p -> projects.add(ProjectData.builder().name(p.getName()).build()));
        return projects;
    }

    public Project getProjectByName(String projectName) {
        return projectRepository.findProjectByName(projectName).orElseThrow(() -> new UsernameNotFoundException("Project not found: " + projectName));
    }

    public boolean addNewProject(EditProjectForm editProjectForm) {
        if (projectRepository.findProjectByName(editProjectForm.getProjectName()).isPresent()) {
            return false;
        } else {
            projectRepository.save(Project.builder().name(editProjectForm.getProjectName()).build());
            return true;
        }
    }

    public boolean removeProject(EditProjectForm editProjectForm) {
        Optional<Project> project = projectRepository.findProjectByName(editProjectForm.getProjectName());

        if (project.isPresent()) {
            projectRepository.delete(project.get());
            return true;
        } else {
            return false;
        }
    }

    public boolean addUserToProject(EditUserInProjectForm editUserInProjectForm) {
        Optional<Project> project = projectRepository.findProjectByName(editUserInProjectForm.getProjectName());
        Optional<User> user = userService.getUserByEmail(editUserInProjectForm.getUserEmail());

        if (project.isPresent() && user.isPresent() && !userExistInProject(project.get().getUsers(), user.get().getEmail())) {
            project.get().getUsers().add(user.get());
            projectRepository.save(project.get());
            return true;
        } else {
            return false;
        }
    }

    public boolean removeUserFromProject(EditUserInProjectForm editUserInProjectForm) {
        Optional<Project> project = projectRepository.findProjectByName(editUserInProjectForm.getProjectName());
        Optional<User> user = userService.getUserByEmail(editUserInProjectForm.getUserEmail());

        if (project.isPresent() && user.isPresent() && userExistInProject(project.get().getUsers(), user.get().getEmail())) {
            project.get().getUsers().remove(user.get());
            projectRepository.save(project.get());
            return true;
        } else {
            return false;
        }
    }

    public List<User> getUsersByProject(String projectName) {
        return getProjectByName(projectName).getUsers();
    }

    public List<UserProjectData> getUsersDataByProject(String projectName) {
        ArrayList<UserProjectData> usersProjectData = new ArrayList<>();

        getUsersByProject(projectName)
                .forEach(u -> usersProjectData.add(UserProjectData.builder().name(u.getLastName() + " " + u.getFirstName()).email(u.getEmail()).jobPosition(u.getJobPosition()).build()));

        return usersProjectData;
    }

    private boolean userExistInProject(List<User> users, String email) {
        return users.stream().anyMatch(u -> u.getEmail().equals(email));
    }

    public List<UserWithEmail> getUsersNotInProject(String projectName) {
        List<String> projectUsersEmails = getProjectByName(projectName).getUsers().stream().map(User::getEmail).collect(Collectors.toList());
        return userService.getUsersWithEmails().stream().filter(u -> !projectUsersEmails.contains(u.getEmail())).collect(Collectors.toList());
    }

    public List<String> getProjectsByLoggedUser() {
        User loggedUser = userService.getLoggedUser();
        Stream<Project> streamOfProjects = projectRepository.findAll().stream();

        if (loggedUser.getRole().getName().equals(Roles.USER.name())) {
            streamOfProjects = streamOfProjects.filter(p -> p.getUsers().contains(loggedUser));
        }

        return streamOfProjects.map(Project::getName).collect(Collectors.toList());
    }
}
