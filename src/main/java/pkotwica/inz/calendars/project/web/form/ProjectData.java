package pkotwica.inz.calendars.project.web.form;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProjectData {
    private String name;
}
