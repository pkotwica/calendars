package pkotwica.inz.calendars.project.web.form;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserProjectData {
    private String name;
    private String email;
    private String jobPosition;
}
