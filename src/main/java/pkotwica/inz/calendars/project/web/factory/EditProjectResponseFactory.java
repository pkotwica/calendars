package pkotwica.inz.calendars.project.web.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class EditProjectResponseFactory extends CalendarsApiResponseFactory {
    private static final String ADD_NEW_PROJECT_FAILED_MESSAGE = "Given name may exists. Try another.";
    private static final String REMOVE_PROJECT_FAILED_MESSAGE = "Remove project goes wrong. Contact with support.";
    private static final String ADD_USER_TO_PROJECT_FAILED_MESSAGE = "Add user to project goes wrong. Contact with support.";
    private static final String REMOVE_USER_FROM_PROJECT_FAILED_MESSAGE = "Remove user from project goes wrong. Contact with support.";

    public static CalendarsApiResponse addByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, ADD_NEW_PROJECT_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse removeByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, REMOVE_PROJECT_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse addUserByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, ADD_USER_TO_PROJECT_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse removeUserByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, REMOVE_USER_FROM_PROJECT_FAILED_MESSAGE);
    }
}
