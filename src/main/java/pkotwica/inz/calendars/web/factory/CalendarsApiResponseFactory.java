package pkotwica.inz.calendars.web.factory;

import org.apache.logging.log4j.util.Strings;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class CalendarsApiResponseFactory {
    private static final String FILL_ALL_DATA_MESSAGE = "Pleas fill all data.";

    public static CalendarsApiResponse fillAllError() {
        return new CalendarsApiResponse(false, FILL_ALL_DATA_MESSAGE);
    }

    protected static CalendarsApiResponse byIsSuccess(boolean isSuccess, String errMessage) {
        if (isSuccess) {
            return new CalendarsApiResponse(true, Strings.EMPTY);
        } else {
            return new CalendarsApiResponse(false, errMessage);
        }
    }
}
