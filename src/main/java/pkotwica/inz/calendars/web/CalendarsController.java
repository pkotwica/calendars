package pkotwica.inz.calendars.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

@Controller
@RequestMapping
public class CalendarsController {

    private final UserService userService;
    private final EventService eventService;
    private final ProjectService projectService;
    private final RoomService roomService;

    public CalendarsController(UserService userService, EventService eventService, ProjectService projectService, RoomService roomService) {
        this.userService = userService;
        this.eventService = eventService;
        this.projectService = projectService;
        this.roomService = roomService;
    }

    @GetMapping
    public String account() {
        return "account-page";
    }

    @GetMapping({"my-calendar/{month}/{year}", "my-calendar/{month}", "my-calendar"})
    public String myCalendar(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, Model model) {
        model.addAttribute("calendarInfo", eventService.getCalendarInfo(month, year));
        model.addAttribute("eventTypes", eventService.getEventTypes());
        model.addAttribute("rooms", roomService.getAllRoomsData());
        return "my-calendar";
    }

    @GetMapping({"users-calendars/{month}/{year}", "users-calendars/{month}", "users-calendars"})
    public String usersCalendars(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String email, Model model) {
        if (email != null) {
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByUser(month, year, email));
            model.addAttribute("eventTypes", eventService.getEventTypes());
        }
        model.addAttribute("users", userService.getUsersWithEmails());
        model.addAttribute("selectedEmail", email);

        return "users-calendars";
    }

    @GetMapping({"projects/{month}/{year}", "projects/{month}", "projects"})
    public String projects(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String project, Model model) {
        if (project != null) {
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByProject(month, year, project));
            model.addAttribute("users", projectService.getUsersDataByProject(project));
        }
        model.addAttribute("projects", projectService.getProjectsByLoggedUser());
        model.addAttribute("projectName", project);

        return "projects";
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}